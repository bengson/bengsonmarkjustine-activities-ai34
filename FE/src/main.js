import Vue from 'vue'
import router from '../src/components/router/index.js'
import App from '../src/components/navigations/Sidenav.vue'


Vue.config.productionTip = false

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

