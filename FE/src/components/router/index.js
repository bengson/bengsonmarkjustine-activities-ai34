import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '../pages/dashboard/dashboard.vue'
import Patron from '../pages/patron/patron.vue'
import Book from '../pages/book/book.vue'
import Settings from '../pages/settings/settings.vue'


Vue.use(Router)

export default new Router({
    routes:[
      {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: '/patron',
        name: 'Patron',
        component: Patron
      },
      {
        path: '/book',
        name: 'Book',
        component: Book
      },
      {
        path: '/settings',
        name: 'settings',
        component: Settings
      }

    ]
})  